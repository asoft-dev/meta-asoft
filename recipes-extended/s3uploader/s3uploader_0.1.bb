SUMMARY = "Simple AWS S3 file upload program"
DESCRIPTION = "Simple AWS S3 file upload program"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRCREV = "ea372ba1a4cb325efc6737b8143521ea09623c97"

SRC_URI = "git://github.com/BLUESINK/${PN}.git"

S = "${WORKDIR}/git"

inherit cmake

do_install(){
    install -d ${D}${bindir}
    install -m 0755 ${PN} ${D}${bindir}
}
