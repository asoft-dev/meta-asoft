#!/bin/sh

echo 120 > /sys/class/gpio/export
echo 119 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio120/direction
echo out > /sys/class/gpio/gpio119/direction
echo 1 > /sys/class/gpio/gpio120/value
echo 1 > /sys/class/gpio/gpio119/value
sleep 0.5
echo 0 > /sys/class/gpio/gpio120/value
sleep 0.5
echo 1 > /sys/class/gpio/gpio120/value
sleep 0.5

stm32flash $@

echo 0 > /sys/class/gpio/gpio119/value
echo 0 > /sys/class/gpio/gpio120/value
sleep 0.5
echo 1 > /sys/class/gpio/gpio120/value


echo 119 > /sys/class/gpio/unexport
echo 120 > /sys/class/gpio/unexport
