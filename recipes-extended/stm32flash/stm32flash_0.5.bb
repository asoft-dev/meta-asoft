SUMMARY = "STM32 Flasher"
DESCRIPTION = "STM32 Flasher"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRCREV = "1f934ae86babdeea47afdfae1d856d5fd5da6c53"

SRC_URI = "git://git.code.sf.net/p/stm32flash/code \
           file://stm32flash.sh"

S = "${WORKDIR}/git"

do_install(){
    make install PREFIX=/usr DESTDIR=${D}
    install -m 0755 ${WORKDIR}/stm32flash.sh ${D}${bindir}
}
